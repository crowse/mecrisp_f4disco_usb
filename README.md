# Mecrisp Stellaris STM32F4Discovery with USB terminal


## Disclaimer
This code is an absolute brute force solution to the need to implement some complex and nitty gritty code in Mecrisp for every processor.

FORTH purists should stop reading at this point since their sensibilities will be deeply offended.

Ok, good, so you are reading further. 

My sensibilities took a deep knock when I figured out how to do this. I believe the HAL libraries supplied by STM to be very badly designed and poorly implemented.
I will back this claim as I go about this task further. There is much to do to correect this and I do not have the resources, time or lifespan to complete it. 
I am currently (Dec 202) 68 years old.

The approach is absolutely pragmatic and introduces some inefficiencies. These can be addressed elsewhere.

Many of the inefficiencies are limited to startup and will have little or no impact on the final system.

void xxx(void) and even uint32_t yyy(parameters...) functions will mostl probably work from mecrisp as long as they do not have unwanted side effects. We need simply to get another dictionary chain provided by C into mecrisp at startup. The experimental vocs() need to become part of the bare bones USB implementation.

It may also be desirable to add either the multitask features, or an interrupt sytle interpreter that gets called whenever a USB packet arrives with a newline. A PC terminal program 'designed for mecrisp-usb' is probably the ultimate answer as we can then communicate from our terminal device to and from Mecrisp very richly. I have a separate backburner project for this, and will get onto it once USB is stable and usable.

The side benefits for me outweigh the inefficiencies in all but the most stringent of environments

1. Get to use the USB virtual COMM port 
2. Get to use Cube to initialise peripherals
3. Can use C and C++ libraries in conjunction with Mecrisp
4. Opens up a whole new suite of applications for quick implementation
5. Can do other things easily e.g. use Ethernet, DMA, FPU etc by letting Cube set them up 

## Aim
The aim of this project is to create USB comms version of Mecrisp for F# F4 and other devices.

The implementation of USB 2.0 on the device reuires that a lot of code eb developed, qulity assured and maintained.

This is at the limits of the resources available to the community, and, few or no real attempts to do this in FORTH have been successful.

With the structure of STM hardware, this work should be portable to other devices in the STM family.

It should also provide a simple menas to use other serial devices such as Wifi, Ethernet and Bluetooth with simple hacks.

There is also the possibility of opening up other endpoints for useful purposes such as debug channels, block storage etc.

1. I will be looking for ways to create this project in such a way that very little change is done to Matthias' files.
2. I intend to make a subdirectory in mecrisp-sources that includes all the working USB and possibly Cube stuff.
3. A common suite of library code is required for mecrisp. This could be built into the same cloneable source
4. Where the C overhead is too great, it may be useful to work from the SFR registers in the debugger
5. It may also be useful to check the output of mecrisp (see) and C++ templates for assembly prototypes
6. Ideally this repository is merged fully with mecrisp-stellaris so you can get everything n one place

## Plan
The initial plan is to simply compile all the code from Mecris and Cube and link it up together.
This did not work as both Cube work from the assumption of a cold boot and initialise all sorts of things.
Additionally, both are of the opinion that they sit at the base of RAM and FLASH and have full control of the CPU and resources.

So the first step is to make a linker map that places Cube and Forth code and RAM in different locations.

We then need to adjust the startup in Mecrisp so that it does not perform initialisation. We will leave this to Cube and/or the FORTH application.

Since both MEcrisp and Cube will be using interrupts, we move the VTOR to RAM during startrup. 
It is convenient to extend VTOR to 128 entries giving us 40 or so at the top for communication.
@TODO There may be a parameter or two passed at startup e.g. to a dictionary in Cube that allows Mecrrisp to access C functions dynamically

## Installing and Compiling 

That being said, if we want a comfortable environment for Mecrisp USB development we are stuck with Eclipse, a 25 year old dragon designed for use with Java.

We will use the STM Cube environment as it builds a lot of what we want on Eclipse

There are a number of idiosyncrasies which we can fortunately work around sufficiently for outr purposes.

The upside is we get ST to develop and maintain aninfrastructure that eases initial development of FORTH tools. We can also use existing C C++ libraries with suitable callbacks to and from FORTH.

The downside is that we need to be aware that a C interrupt service routine may have a few dozen levels of procedure calls. A 50 byte stack will not suffice.

Keeping FORTH memory separate from C memory, especially in loght of malloc() and stack frames may dictate that we migrate FORTH to using the PSP instead of the MSP. This will have implications on how we perform some tasks.

### Etiquette
- Please clone the repository, then branch it before making changes / additions.
- After discussion, the branches can and will be merged back to the HEAD.
- Pull before you change, Commit often, Push milestones.
- Put meaningful messages in each commit.
- It would be more productive to Branch rather than Fork.
- Please discuss on the IRC and/or boards so that we can all go forward together.
- Please thank MAtthias fro providing an excellent tool in mecrisp.
- Please thank Terry for writing up the manual

### Download and Import

The simplest solution I have found is 
- Create a workspace in Cube
- Create a Cube project for your processor or board
- Use cube to configure the peripherals you want to use e.g. USB
- Create or modify files in a folder <mecrisp>/mecrisp-stellaris-source/<Your processor>-usb
- Import the mecrisp sources to this project
- Notify Cube where to find Mecrisp source when it does the assembly 
- Modify the linker script to better manage our memory

These are the steps I follow - yes, I know its long, but that is Eclipse.
Also note that STM have removed git features from their cube offering, I currently use git-bash.

1. Clone this repository 
2. Open the STM Cube IDE
3. Create a workspace (This keeps Mecrisp separate from your other work)
3. Import -> General -> projects from Filesystem or Archive
           Point to your clone branch
4. Point into the mecrisp download till you get to stm32f407-ra-usb (or whatever)
5. Click finish
6. Open the project and point to Src/Core/mecrisp
7. <Shift> Click on all files other than mecrisp-stellaris-stm32f<407>.s
8. Right click on one of the highlighted files -> Resource Configurations -> Exclude from Build
       Select Release and Build then Apply
10. Right click on the project ->Properties -> C/C++ General -> Paths And Symbols -> includes 
           Add a workspace include to the F407-ra-usb/Core/mecrisp folder
           This enables the assembler to find the files we just excluded
12. Build the project
           You should geta clean compile
12. Debug it as a STM Cube project

Enjoy

## Making changes or adding a cpu

Get a working copy at least to the stage in Download and Import above.

1. Copy one of the existing -ra-usb directories to one for your cpu
2. Edit and make changes as you see fit
3. Create a Cube project 
4. Import the directory you just made into your project as above
5. Exclude the files noted above
6. Add an include to point to the excluded files
7. Compile the project
8. Test
9. Discuss on IRC so we can collaborate on issues you may have
9. Publish - we all want to use that CPU 

#### Writing the Link Script

As of this time (3 DEcember 2020) Very little needs to be done to the linker script.
The main change is to move the memory constants out of the assembly and into the linker.
This makes it easier to port to a similar processor.

Ideally we have 
- one set of assembly sources which Matthias and co maintain
- One Cube project and C/C++ source code set per CPU family
- a linker script per processor model

The GCC linker is a powerful tool and provides almost all we need to be able to link a Cube program to an Assembly program.
Arm have also been very kind to us by standardising the way we use the registers in the CPU.
As long as we observe these guidelines and don't do anything silly, it should all work with few modifications.
Matthias has been very kind in sticking to the rules and I foresee very few changes to the code.

#### Resources
The following links are useful when changing the linker script
1. https://www.keil.com/support/docs/4014.htm
2. https://www.keil.com/support/docs/4014.htm
3. https://www.keil.com/support/docs/4014.htm




## Memory Layout
This is a brute force approach an dwill remain as such, possibly indefinitely.
I need a stable, tried and tested working copy figure out how to get Eclipse to asseemble Mecrisp into Cube). 
@TODO: Create and expose symbols in Mecrisp to access these locations from the linker
      This may be done either via globals or 

### Flash
    Cube gets the first half (512Kb)  
	Mecrisp gets the Top half (512Kb)
	
### RAM (Check the .ld files for your CPU)
    Cube gets the first third (64Kb)
	Shared RAM at second third. The VTOR takes the first 128*4 bytes
	Mecrisp gets the top 64Kb
	

### Changed Files

#### memap
@TODO: With a change to Makefile this file becomes obsolete and all memory management can be done in one place
@TODO: STM docs say that the F4 Discovery has 196Kb RAM, I haven't been able to get beyond 128K, what am I doing wrong?

#### Makefile
@TODO to change the memory map to point to our CUBE memory model, Memory layout can then be achieved in one place
#### mecrisp-stellaris-<cpu>.s
 - defines some constants that we need to change. We can get these from the linker.
 - remove vectors.s and interrupts.s, we will do this in FORTH as the VTOR is in Ram 
 
#### catchFlashPointers.s
     Initially changed to remove sp and psp initialisation, but found we need it
     SP and heap (RAM_HERE) need to be in the same place
#### terminal.s
	Simply need to rewrite the 4 primitives to use our USB buffers.
	PRototypes exist in main.c, but first we test we can get a USART mecrisp working with USB loopback 
	We extend Matthias' approach and write a usb-terminal.s which uses buffers in shared memory
	Better at this stage to provide both USART and USB copies for easier debugging
	The primitives can be written in C
	
#### Interrupts and vectors
    I have a working mecrisp library that makes these files obsolete 
    - moves the VTOR to RAM
    - Provides a list of IRQn constants per processor
    - Handles all initialisation, enabling, disabling, revectoring, NVIC on the fly
    - Does not require a recompile of mecrisp to add new vectors
    - Probably takes less space
    
      I will publish this code in htis archive once I have the USB working
        
        
All changes are commented with @CJR tags


## TODO:
### Get a clean compile
    Achieved 2 December 2020

### Manage the ROM space
   - DictionaryNext expects an $FF at the end of built in Flash memory
   - Either adjust the linker script / Assembly to place it more effectively
   - Or place an FF at the end of Assembly source as a dummy hidden dictionary entry
   - Or place mecrisp in its own Section and allow space for new words - probably the safest
   - crefully examine the .list file to check memory placement
    Achieved 4 December, we can traverse Flash dictionary
    
### Setup the RAM dictionary
    4 December 2020 - Stuck at the point flash traverse reaches end of flash dictionary
    It looks at RAM dictionary and dies
    Either my ram dictionary pointer is messed or I need to initialise a dummy dictionary entry 
    Perhaps Matthias or someone can help?
   
### Convert to a single linker script
   This is implicit in the current project config
   
### Enable mecrisp sources to be managed independant of Cube
   This appears to be exactly what we have. 
   Mecrisp sources are unmodified
   We build a mecrisp<****>-usb version where we play with startup, key and emit
   Achieved 2 December 2020
   
### Achieve basic functionality 
     Want to be able to see USB comm port working in C and Mecrisp working over standard USB interface
### Investigate the classic hooks 
- emit, key, ?emit, ?key found in terminal.s
- Revector these on the fly from USART and finally init.
- Compile in USB variants and remove USART variants
### Check the function
### Improve the communication speed and accuracy
### Integrate to an IDE designed for FORTH
### Conquer the world
### Rewrite parts of Mecrisp
- Change from an key/emit model to expect/straight/type model as per polyFORTH circa 1980. (Gosh, that's 40 years ago!) 
### Rewrite the outer interpreter to use a line of text rather than a character 
### Provide a parameter from the C module to steer the initialisation
### Provide a parameter from C to locate a mecrisp compatible dictionary and words
### Provide hooks in the C/C++ code to interesting functionality
### Provice a C function to reflash a .bin of mecrisp into the appropriate memory
This could be called from FORTH, or done through a secondary DFU like endpoint in the USB interface. 


## Status
### 9,10 December 2020
- Got a working verrsion that compiles to FLASH and RAM using USART
- USB loopback device working with this in place
- Problem is the remapping of FLASH in M3 cores
- Resorted to assembly equates in mecrisp....s  file coupled to Flash 
- Needs to be worked on some more
- may not be fully possible without making a relocation loader
- Need to always test after st-flash erase otherwise old versions may give unexplained results
- Moving on for now to USB comms - should be as simple as using the primitives I have

First tests of USB failed .... whilst callback is functional at same time as mecrisp USART, no callbacks on 
using the key/emit routines


### 7,8 December 2020
- A lot of assistance from mecrisp (Matthias) tp (Terry) and crest on IRC
- Also looked at Peter's code for the STM32WB55 board
- Made some changes to the linker map 
- Got an OK prompt and compile to ram 
- compiletoflash now breaks because flash-here is at end of code and needs to be at a page boundary
- there may also be a problem with some c library code being linked into same space as FORTH

- ST32WB55 code works from mecrisp sourceforge, but not from spyr.ch although may have ok. prompt on bluetooth

Very near a solution 

### 3 December 2020
- Moved stack to MEcrisp Ram. 
- Got rid of some errors in the memmap
- Lots of single stepping through the setup code
- Seem to be able to traverse the Flash during setup. This initialises variables etc.
- Final test crashed on jump to RAM dictionary

### 2 December 2020
- Initial compilation done
- Branch from C to Mecrisp done
- Started changing Mecrisp to a shifted memory model

- The subproject was not working as I could not get the indirect jump through the Assembly Reset vector in VTOR to work
- The Mecrisp source is now included in a subdirectory so everything compiles to a singl ebinary
- DictionayNext is not stopping at the end of flash as expected
- This is because the linker is not linking it at the end of ROM section



### 20 November 2020
   - Testing with code from 18th showed memory management  problems and hard faults
         Probably because c and FORTH are not communicating their use of RAM
         A quick fix is to manage them using linker scripts
         RAM - create 3 areas of 64Kb each - C Ram, shared RAM , FORTH ram
               shared ram is intended for VTOR, USB buffers and inter language communication blocks
               can also use as a FORTH Blocks storage
         FLASH  - 2 areas 512Kb each one for C one for Mecrisp
    - With this arrangement, the two systems should be Ok to function in same machine
      may need to push/pop a few registers when we go between them

   - Moved project files to workspace-mecrisp-usb
            This means the entire workspace is under git control
            We just need to pull it, almost regardless of our platform
   - Linked Mecrisp distribution
            For ease of use
   - Created a new Mecrisp target for F407-ra-usb
           Adjusted the linker script for F407-ra-usb as above
           This links mecrisp at 512Kb above C librarys
           As we have 1024Kb Flash this should be Ok          
          This loads compiles and appears to have the addressing Ok based on list and mammap files
          Probably need to chain indirectly to reset, 
               i.e. first memory in FORTH is actually the VTOR memory
                    first location  in this points to Reset ode in MEcrisp
               we need  something like 
                           __attribute__ ((section("mecrisp_flash) extern void *forthvtor())));
                            *vtor[0](); 


### 18 November 2020

	- Initial creation of project and inclusion of sources known to be necessary
	- Initial compile - failed on the macros. Probably a result of the way the sources normally include each other.
	- Must either stop Cube from including all theese files
	- or create a file of macros that includes in each source file and stop assemnbly of the main program the way MAtthias doe

## Memory Layout
This is a brute force approach an dwill remain as such, possibly indefinitely.
I need a stable, tried and tested working copy figure out how to get Eclipse to asseemble Mecrisp into Cube). 
@TODO: Create and expose symbols in Mecrisp to access these locations from the linker
      This may be done either via globals or 

### Flash
    Cube gets the first half (512Kb)  
	Mecrisp gets the Top half (512Kb)
	
### RAM (Check the .ld files for your CPU)
    Cube gets the first third (64Kb)
	Shared RAM at second third. The VTOR takes the first 128*4 bytes
	Mecrisp gets the top 64Kb
	

### Changed Files

#### memap
@TODO: With a change to Makefile this file becomes obsolete and all memory management is done in one place
#### Makefile
need to change the memory map to point to our CUBE memory model, Memory layout is achieved in one place
#### mecrisp-stellaris-<cpu>.s
 - defines some constants that we need to change. We can get these from the linker.
 - remove vectors.s and interrupts.s, we will do this in FORTH as the VTOR is in Ram 
 
#### catchFlashPointers.s
     Remove initialisation of sp and psp.
	 Perhaps Matthias can move this code to the <cpu.s> files 

#### terminal.s
	Simply need to rewrite the 4 primitives to use our USB buffers.
	We extend Matthias' approach and write a usb-terminal.s which uses buffers in shared memory
	May be better at this stage to provide both USART and USB copies for easier debugging
	
All changes are commented with @CJR tags 

## TODO:

### Get a clean compile
### Achieve basic functionality 
     Want to be able to see USB comm port working in C and Mecrisp working over standard USB interface
### Investigate the classic hooks 
- emit, key, ?emit, ?key found in terminal.s
- Revector these on the fly from USART and finally init.
- Compile in USB variants and remove USART variants
### Check the function
### Improve the communication speed and accuracy
### Integrate to an IDE designed for FORTH
### Conquer the world
### Rewrite parts of Mecrisp
- Change from an key/emit model to expect/straight/type model as per polyFORTH circa 1980. (Gosh, that's 40 years ago!) 
- Rewrite the outer interpreter to use a line of text 

## Using the Link Script
##Usage
The GCC linker is a powerful tool and provides almost all we need to be able to link a Cube program to an Assembly program.
Arm have also been very kind to us by standardising the way we use the registers in the CPU.
As long as we observe these guidelines and don't do anything silly, it should all work with few modifications.
Matthias has been very kind in sticking to the rules and I foresee very few changes to the code.



## Resources
The following links are useful
1. https://www.keil.com/support/docs/4014.htm
2. https://www.keil.com/support/docs/4014.htm
3. https://www.keil.com/support/docs/4014.htm

## Status
### 2 December 2020
- Initial compilation done
- Branch from C to Mecrisp done
- Started changing Mecrisp to a shifted memory model

	
-----------------------------------------------------------------------------------------------------------------------------------------	


## Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
