@
@    Mecrisp-Stellaris - A native code Forth implementation for ARM-Cortex M microcontrollers
@    Copyright (C) 2013  Matthias Koch
@
@    This program is free software: you can redistribute it and/or modify
@    it under the terms of the GNU General Public License as published by
@    the Free Software Foundation, either version 3 of the License, or
@    (at your option) any later version.
@
@    This program is distributed in the hope that it will be useful,
@    but WITHOUT ANY WARRANTY; without even the implied warranty of
@    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@    GNU General Public License for more details.
@
@    You should have received a copy of the GNU General Public License
@    along with this program.  If not, see <http://www.gnu.org/licenses/>.
@

.syntax unified
.cpu cortex-m4
.thumb

.section mecrisp_flash

@ -----------------------------------------------------------------------------
@ Swiches for capabilities of this chip
@ -----------------------------------------------------------------------------

.equ charkommaavailable, 1
.equ registerallocator, 1

@ -----------------------------------------------------------------------------
@ Start with some essential macro definitions
@ -----------------------------------------------------------------------------

.include "../common/datastackandmacros.s"

@ -----------------------------------------------------------------------------
@ Speicherkarte für Flash und RAM
@ Memory map for Flash and RAM
@ -----------------------------------------------------------------------------

@ Konstanten für die Größe des Ram-Speichers
@CJR   GEt these from the linker
@ .equ RamAnfang, 0x20000000 @ Start of RAM          Porting: Change this !
@ .equ RamAnfang,   0x20010000 @ End   of RAM. 128 kb. Porting: Change this !
@ .equ RamEnde,   0x20020000

.equ RamAnfang, _RamAnfang @ Start of RAM          Porting: Change this in memmap !
.equ RamEnde,   _RamEnde   @ End   of RAM. 128 kb. Porting: Change this in memmap !

@ Konstanten für die Größe und Aufteilung des Flash-Speichers
@ We'd really like to pick these up from the linker, but they get remaped by the ARM
@
@.equ Kernschutzadresse,     0x00010000 @ Darunter wird niemals etwas geschrieben ! Mecrisp core never writes flash below this address.
@.equ FlashDictionaryAnfang, 0x00010000 @ 32 kb für den Kern reserviert...          32 kb Flash reserved for core.
@.equ FlashDictionaryEnde,   _FlashDictionaryEnde  @ Porting: Change this !
@.equ Backlinkgrenze,        _RamAnfang  @ Ab dem Ram-Start.

@.equ Kernschutzadresse,     _FlashCodeEnde
@.equ FlashDictionaryAnfang, _FlashDictionaryAnfang  @ Where user flash begins

@ .equ FlashDictionaryAnfang, 0x08020000 @ 32 kb für den Kern reserviert...          32 kb Flash reserved for core.
@ .equ FlashDictionaryEnde,   _FlashDictionaryEnde  @ Porting: Change this !
@ .equ Kernschutzadresse,      0x8025600
@ .equ FlashDictionaryAnfang,  0x8025600  @ Where user flash begins

.equ FlashDictionaryAnfang, 0x0020000 @ 32 kb für den Kern reserviert...          32 kb Flash reserved for core.
.equ FlashDictionaryEnde,   0x0040000  @ Porting: Change this !
.equ Kernschutzadresse,     0x0025600
.equ FlashDictionaryAnfang, 0x0025600  @ Where user flash begins


@.equ FlashDictionaryEnde,   _FlashDictionaryEnde  @ Porting: Change this !
.equ Backlinkgrenze,        _RamAnfang  @ Ab dem Ram-Start.


@ -----------------------------------------------------------------------------
@ Anfang im Flash - Interruptvektortabelle ganz zu Beginn
@ Flash start - Vector table has to be placed here
@ -----------------------------------------------------------------------------
.text    @ Hier beginnt das Vergnügen mit der Stackadresse und der Einsprungadresse
.include "./vectors.s" @ You have to change vectors for Porting !

@ -----------------------------------------------------------------------------
@ Include the Forth core of Mecrisp-Stellaris
@ -----------------------------------------------------------------------------

.include "../common/forth-core.s"

@ -----------------------------------------------------------------------------
.global Reset
Reset: @ Einsprung zu Beginn
@ -----------------------------------------------------------------------------
   @ Initialisierungen der Hardware, habe und brauche noch keinen Datenstack dafür
   @ Initialisations for Terminal hardware, without Datastack.
   @ CJR we don't need to initialise any chip stuff as Cube has done that for us
   @ bl uart_init

   @ Catch the pointers for Flash dictionary debug
   ldr r0, =Kernschutzadresse
   ldr r1, =FlashDictionaryAnfang
   ldr r2, =FlashDictionaryEnde
   ldr r3, =RamAnfang
   ldr r4, =RamEnde

   @ CJR moved these here since we don't want to do them when we run from USB
   ldr r0, =returnstackanfang
   mov sp, r0
   ldr psp, =datenstackanfang

   @CJR .include "../common/catchflashpointers.s"
 .include "./catchflashpointers.s"

   welcome " for STM32F407 by Matthias Koch, Cube and USB by Chris Rowse"

   @ Ready to fly !
   .include "../common/boot.s"

   @ .section text
