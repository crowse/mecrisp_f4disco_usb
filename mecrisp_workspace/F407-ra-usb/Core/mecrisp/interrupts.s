@
@    Mecrisp-Stellaris - A native code Forth implementation for ARM-Cortex M microcontrollers
@    Copyright (C) 2013  Matthias Koch
@
@    This program is free software: you can redistribute it and/or modify
@    it under the terms of the GNU General Public License as published by
@    the Free Software Foundation, either version 3 of the License, or
@    (at your option) any later version.
@
@    This program is distributed in the hope that it will be useful,
@    but WITHOUT ANY WARRANTY; without even the implied warranty of
@    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@    GNU General Public License for more details.
@
@    You should have received a copy of the GNU General Public License
@    along with this program.  If not, see <http://www.gnu.org/licenses/>.
@

@ Routinen für die Interrupthandler, die zur Laufzeit neu gesetzt werden können.
@ Code for interrupt handlers that are exchangeable on the fly

@------------------------------------------------------------------------------
@ Alle Interrupthandler funktionieren gleich und werden komfortabel mit einem Makro erzeugt:
@ All interrupt handlers work the same way and are generated with a macro:
@------------------------------------------------------------------------------
interrupt dma1_stream0
interrupt dma1_stream1
interrupt dma1_stream2
interrupt dma1_stream3
interrupt dma1_stream4
interrupt dma1_stream5
interrupt dma1_stream6

interrupt dma2_stream0
interrupt dma2_stream1
interrupt dma2_stream2
interrupt dma2_stream3
interrupt dma2_stream4
interrupt dma2_stream5
interrupt dma2_stream6
interrupt dma2_stream7

interrupt exti0
interrupt exti1
interrupt exti2
interrupt exti3
interrupt exti4
interrupt exti9_5
interrupt exti15_10

interrupt fpu

interrupt adc

interrupt tim1_brk_tim9
interrupt tim1_up_tim10
interrupt tim1_trg_com_tim11
interrupt tim1_cc


interrupt tim2
interrupt tim3
interrupt tim4
interrupt tim5
interrupt tim6_dac
interrupt tim7

interrupt rtc_alarm
interrupt otg_wkup

interrupt tim8_brk_tim12
interrupt tim8_up_tim13
interrupt tim8_trg_com_tim14
interrupt tim8_cc

interrupt uart4
interrupt uart5
interrupt usart6


@------------------------------------------------------------------------------

